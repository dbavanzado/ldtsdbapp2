package es.ldts.dbapp2;

import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

//ARREGLA ESTA ACTIVIDAD:
//1-. Haz que en en el TextView2 aparezca el la fecha actual.
//2-. Haz que el botón Volver funcione
//Para esto tendrás que echar un vistazo a los archivos asociados:
//El Layout y el código de MyDatePickerFragment

public class DatePickerActivity extends AppCompatActivity {

    public static String fechaactual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_picker);
        fechaactual="";
    }
    public void showDatePicker(View v) {
        DialogFragment newFragment = new MyDatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "date picker");
    }

    public void goBack(View v) {
        Intent intent = new Intent(this, MainActivity.class);// New activity
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish(); // Call once you redirect to another activity
    }

}